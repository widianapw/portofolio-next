import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import {Link} from "react-scroll";
import {
    FaLinkedin,
    FaInstagram,
    FaWhatsapp,
    FaBook,
    FaLaptopCode,
} from "react-icons/fa";
import PrimaryButton from "../components/PrimaryButton";
import TypewriterComponent from "typewriter-effect";
import ServiceCard from "../components/ServiceCard";
import Card from "../components/Card";
import SecondaryButton from "../components/SecondaryButton";
import Section from "../components/Section";

export default function Home() {
    return (
        <div className="flex flex-col bg-background h-full w-full font-body text-primary">
            {/* Header */}
            <header
                className="flex bg-white flex-row justify-start items-center py-6 px-6 md:px-20 lg:px-48 sticky top-0 z-40 shadow-sm text-primary">
                <div className=" text-primary font-display text-xl md:text-3xl font-extrabold flex-1 tracking-tight">
                    <p>widianapw</p>
                </div>
                <nav className="hidden md:flex">
                    <ul className="flex lg:flex-row justify-end space-x-8 font-body font-semibold tracking-tight">
                        <li className=" cursor-pointer">
                            <Link
                                activeClass="text-yellow-400"
                                to="home"
                                spy={true}
                                smooth={true}
                                offset={-100}
                                duration={500}
                            >
                                Home
                            </Link>
                        </li>

                        <li className=" cursor-pointer">
                            <Link
                                activeClass="text-yellow-400"
                                to="about"
                                spy={true}
                                smooth={true}
                                offset={-100}
                                duration={500}
                            >
                                About
                            </Link>
                        </li>

                        <li className=" cursor-pointer">
                            <Link
                                activeClass="text-yellow-400"
                                to="experience"
                                spy={true}
                                smooth={true}
                                offset={-100}
                                duration={500}
                            >
                                Experience
                            </Link>
                        </li>

                        <li className=" cursor-pointer">
                            <Link
                                activeClass="text-yellow-400"
                                to="works"
                                spy={true}
                                smooth={true}
                                offset={-100}
                                duration={500}
                            >
                                Works
                            </Link>
                        </li>
                    </ul>
                </nav>
            </header>

            {/* Cover */}
            <section
                className="flex flex-col py-24  md:py-36 justify-center items-center font-body text-primary tracking-tight"
                id="home"
            >
                <div className="absolute w-full max-w-xl">
                    <div
                        className="absolute top-0 left-4 w-48 h-48 md:w-72 md:h-72 bg-purple rounded-full mix-blend-multiply filter blur-xl opacity-70 animate-blob"></div>
                    <div
                        className="absolute top-0 right-12 w-48 h-48 md:w-72 md:h-72 bg-yellow-300 rounded-full mix-blend-multiply filter blur-xl opacity-70 animate-blob animation-delay-2000"></div>
                    <div
                        className="absolute -bottom-8 left-20 w-48 h-48 md:w-72 md:h-72 bg-pink rounded-full mix-blend-multiply filter blur-xl opacity-70 animate-blob animation-delay-4000"></div>
                </div>

                <div
                    className=" relative bg-white flex flex-col p-10 justify-center items-center rounded-3xl bg-white shadow-xl">
                    <Image
                        src="/images/avatar.svg"
                        width={120}
                        height={120}
                        className="relative z-0"
                    />
                    <p className=" font-extrabold mt-4 font-body text-2xl text-center">
                        Widiana Putra
                    </p>
                    <div className=" flex flex-row space-x-2 items-center justify-center">
                        <TypewriterComponent
                            options={{
                                strings: [
                                    "Software Developer",
                                    "Mobile Application Developer",
                                    "Website Developer",
                                ],
                                autoStart: true,
                                loop: true,
                            }}
                        />
                    </div>

                    <div className=" flex flex-row space-x-4 mt-6">
                        <a href="https://linkedin.com/in/widiana-putra-699351161">
                            <FaLinkedin className="hover:scale-125 hover:cursor-pointer"/>
                        </a>

                        <a href="https://www.instagram.com/widianapw/">
                            <FaInstagram className="hover:scale-125 hover:cursor-pointer"/>
                        </a>

                        <a href="https://wa.me/6282146456432">
                            <FaWhatsapp className="hover:scale-125 hover:cursor-pointer"/>
                        </a>
                    </div>
                    <a href="https://campsite.bio/widianapw">
                        <PrimaryButton text="Say hello!" className="mt-6"/>
                    </a>
                </div>

                {/* </div> */}
            </section>

            {/* about */}
            <Section className="px-48 py-20 flex flex-col flex-1 " id="about">
                <div className=" absolute ">
                    <Image src="/images/dots-bg.svg" width={60} height={60}/>
                </div>

                <h2 className=" font-extrabold text-3xl mt-4">About Me</h2>
                <div className="flex flex-row mt-6 p-0 lg:p-6 space-x-0 lg:space-x-10 items-start">
                    <div className="hidden lg:flex">
                        <Image src="/images/avatar.svg" width={200} height={200}/>
                    </div>

                    <div
                        className=" bg-white flex-grow rounded-2xl shadow-lg p-8 flex flex-col"
                        data-aos="fade-up"
                    >
                        <div className="mb-6 flex justify-center md:hidden">
                            <Image src="/images/avatar.svg" width={120} height={120}/>
                        </div>

                        <p>
                            Working on Mobile and Web Application Development. <br/>
                            1. Timedoor Indonesia as Mobile Application Developer <br/>
                            2. Lovree Invitation as Owner and developer <br/>I have done some project
                            about mobile and website apps and always trying to keep updates
                            with new technology. Interest in Laravel, React, Kotlin, and
                            Flutter framework
                        </p>
                        <div className="mt-6 flex justify-center md:justify-start">
                            <a href="https://linkedin.com/in/widiana-putra-699351161">
                                <PrimaryButton text="View Linked In"/>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mt-8 ">
                    <ServiceCard className="bg-purple">
                        <p className=" text-white text-2xl font-body font-bold">
                            Mobile Application Development
                        </p>
                        <p className="text-white mt-2">
                            <ul>
                                <li>React Native</li>
                                <li>Flutter</li>
                                <li>Kotlin</li>
                            </ul>
                        </p>
                    </ServiceCard>

                    <ServiceCard className="bg-yellowPrimary">
                        <p className=" text-primary text-2xl font-body font-bold">
                            Website Application Tech
                        </p>
                        <p className="text-primary mt-2">
                            <ul>
                                <li>Laravel</li>
                                <li>NextJS</li>
                                <li>React JS</li>
                                <li>Wordpress</li>
                            </ul>
                        </p>
                    </ServiceCard>

                    {/* <ServiceCard className="bg-pink">
            <p className=" text-white text-2xl font-body font-bold">
              Website Builder Development
            </p>
            <p className="text-white mt-2"></p>
          </ServiceCard> */}
                </div>
            </Section>

            {/* Experience */}
            <Section className=" " id="experience">
                <div className=" absolute">
                    <Image src="/images/dots-bg.svg" width={60} height={60}/>
                </div>
                <h2 className=" font-extrabold text-3xl mt-4">Experience</h2>
                <div className=" grid grid-cols-1 md:grid-cols-2 gap-4 mt-8">
                    <div
                        className="bg-white rounded-xl shadow-lg p-8 flex-1"
                        data-aos="fade-up"
                    >
                        <div className="flex flex-row items-center space-x-2">
                            <FaBook/>
                            <p className=" text-xl font-bold">Study</p>
                        </div>

                        <div className="mt-4 p-2 flex-col flex">
                            <p className=" text-sm text-gray tracking-wider font-bold">
                                2017 - 2021
                            </p>
                            <p className=" font-bold text-lg">
                                Bachelor of Information Technology
                            </p>
                            <p className=" text-md font-thin">Udayana University</p>
                        </div>
                    </div>

                    <div
                        className="bg-white rounded-xl shadow-lg p-8 flex-1"
                        data-aos="fade-up"
                    >
                        <div className="flex flex-row items-center space-x-2">
                            <FaLaptopCode/>
                            <p className=" text-xl font-bold">Job</p>
                        </div>

                        <div className="mt-4 p-2 flex-col flex">
                            <p className=" text-sm text-gray tracking-wider font-bold">
                                2021 - Present
                            </p>
                            <p className=" font-bold text-lg">Software Developer</p>
                            <p className=" text-md font-thin">Timedoor Indonesia</p>
                        </div>

                        <div className="mt-4 p-2 flex-col flex">
                            <p className=" text-sm text-gray tracking-wider font-bold">
                                2020 - 2021
                            </p>
                            <p className=" font-bold text-lg">Software Quality Assurance</p>
                            <p className=" text-md font-thin">Timedoor Indonesia</p>
                        </div>
                    </div>
                </div>
            </Section>

            <Section className=" " id="works">
                <div className=" absolute">
                    <Image src="/images/dots-bg.svg" width={60} height={60}/>
                </div>
                <h2 className=" font-extrabold text-3xl mt-4">Recent Works</h2>
                <div className="grid grid-cols-1 md:grid-cols-3 gap-4 mt-8">
                    <Card>
                        <p className=" text-2xl font-bold">Dewi Sri House</p>
                        <p className="mt-2">
                            Dewi sri house is villa in bali.
                        </p>
                        <a href="https://dewisrihouse.com/">
                            <SecondaryButton text="Open Website" className=" mt-4 "/>
                        </a>
                    </Card>

                    <Card>
                        <p className=" text-2xl font-bold">Kelan Beach</p>
                        <p className="mt-2">
                            Kelan beach is a Company Profile Wesbsite for Kelan Beach Tourism.
                        </p>
                        <a href="https://kelanbeach.com/">
                            <SecondaryButton text="Open Website" className=" mt-4 "/>
                        </a>
                    </Card>

                    <Card>
                        <p className=" text-2xl font-bold">Lovree Invitation</p>
                        <p className="mt-2">
                            Lovree is website for invitation of wedding event. Lovree has
                            some elegant invitation template. Lovree is the builder of the
                            wedding invitation website, the customer only need to input their
                            data on system,
                        </p>
                        <a href="https://lovree.com/">
                            <SecondaryButton text="Open Website" className=" mt-4 "/>
                        </a>
                    </Card>

                    <Card>
                        <p className=" text-2xl font-bold">Pande Putri Admin Web</p>
                        <p className="mt-2">
                            Pande Putri is a supermarket based in bali that provides various types of goods ranging from
                            basic needs,
                            household needs, electronics and other necessities.
                        </p>
                    </Card>
                    <Card>
                        <p className=" text-2xl font-bold">USDM Open Access App</p>
                        <p className="mt-2">
                            OpenAccess adalah sebuah platform agregator perusahaan jaringan dengan perusahaan ISP untuk
                            bisa saling berkolaborasi membuka akses sampai ke pelosok. </p>
                        <a href="https://play.google.com/store/apps/details?id=co.openaccess.app">
                            <SecondaryButton text="Open App" className=" mt-4 "/>
                        </a>
                    </Card>

                    <Card>
                        <p className=" text-2xl font-bold">eFishien</p>
                        <p className="mt-2">
                            eFishien is an app created to help you trade fish. This application has various features to
                            meet your fish sales needs.</p>
                        <a href="https://play.google.com/store/apps/details?id=com.efishien.app">
                            <SecondaryButton text="Open App" className=" mt-4 "/>
                        </a>
                    </Card>


                    <Card>
                        <p className=" text-2xl font-bold">Mitra Bevander</p>
                        <p className="mt-2">
                            Mitra Bevander is app for managing the merchant of the bevander company (E-Commerce Admin).
                            Customer app is bevander.
                        </p>
                        <a href="https://play.google.com/store/apps/details?id=net.timedoor.admin.expressappadmin.district">
                            <SecondaryButton text="Open App" className=" mt-4 "/>
                        </a>
                    </Card>

                    <Card>
                        <p className=" text-2xl font-bold">Appmu Admin App</p>
                        <p className="mt-2">
                            Appmu Admin is an Android Aplication that used for the e-commerce
                            administrator. Appmu is an E-commerce template application.
                        </p>
                        <a href="https://play.google.com/store/apps/details?id=net.timedoor.admin.expressappadmin">
                            <SecondaryButton text="Open App" className=" mt-4 "/>
                        </a>
                    </Card>

                </div>
            </Section>
            <footer
                className=" bg-background  py-8 flex flex-col items-center border-t border-zinc-300 justify-center text-center text-xs font-bold">
                <p>Copyright &copy; 2021 Widianapw</p>
            </footer>
        </div>
    );
}
