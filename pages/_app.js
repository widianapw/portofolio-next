import "aos/dist/aos.css";
import Aos from "aos";
import AOS from "aos";
import Head from "next/head";
import { useEffect } from "react";
import "../styles/globals.css";
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
      apiKey: "AIzaSyBBFiI0pKdJH7Z8RkS04o7py7LuYLLemdk",
      authDomain: "widianapw-a8c43.firebaseapp.com",
      projectId: "widianapw-a8c43",
      storageBucket: "widianapw-a8c43.appspot.com",
      messagingSenderId: "475433737929",
      appId: "1:475433737929:web:be6604afc2915a428cb718",
      measurementId: "G-TGSK1GWHW1",
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
    AOS.init({
      duration: 1500,
    });
  }, []);
  return (
    <>
      <Head>
        <title>Widianapw - Portfolio</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
