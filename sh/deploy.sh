#!/bin/bash

export PROJECT_DIR="public_html/pm2test"

ssh pandeputri@13.214.113.182 <<'ENDSSH'
  echo "Connected to server"
  cd public_html/pm2test/portfolio
  git pull origin main

  exit
ENDSSH
