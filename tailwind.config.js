module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      body: ["Rubik", "sans-serif"],
      display: ["Rubik", "sans-serif"],
    },

    extend: {
      colors: {
        primary: "#353353",
        background: "#f9f9ff",

        blue: "#1fb6ff",
        purple: "#6c6ce5",
        pink: "#ff4c60",
        orange: "#ff7849",
        green: "#13ce66",
        yellowPrimary: "#f9d74c",
        "gray-dark": "#273444",
        gray: "#8492a6",
        "gray-light": "#d3dce6",
      },

      animation: {
        blob: "blob 7s infinite",
      },
      keyframes: {
        blob: {
          "0%": {
            transform: "translate(0px, 0px) scale(1)",
          },
          "33%": {
            transform: "translate(30px, -50px) scale(1.1)",
          },
          "66%": {
            transform: "translate(-20px, 20px) scale(0.9)",
          },
          "100%": {
            transform: "tranlate(0px, 0px) scale(1)",
          },
        },
      },
    },
  },
  plugins: [],
};
