export default function Card(props) {
    const style =
        "flex flex-col bg-white p-8 rounded-lg shadow-lg " + " " + props.className;
    return (
        <div className={style} data-aos="fade-up">
            {props.children}
        </div>
    );
}
