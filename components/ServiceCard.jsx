export default function ServiceCard(props) {
  const style =
    "flex flex-col bg-white p-8 rounded-lg shadow-lg cursor-default" +
    " " +
    props.className;
  return <div className={style}>{props.children}</div>;
}
