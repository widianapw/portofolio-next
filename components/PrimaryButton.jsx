export default function PrimaryButton(props) {
  const defStyle =
    "bg-pink px-6 py-2 rounded-3xl font-body text-white font-bold hover:scale-110 transition duration-300 ease-in-out cursor-pointer" +
    " " +
    props.className;
  return <button className={defStyle}>{props.text}</button>;
}
