export default function Section(props) {
  const styles =
    "px-8 md:px-24 lg:px-48 py-12 lg:py-20 flex flex-col flex-1" +
    " " +
    props.className;
  return (
    <section className={styles} id={props.id}>
      {props.children}
    </section>
  );
}
